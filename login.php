<?php
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // Handle preflight OPTIONS request
    header('Access-Control-Allow-Origin: http://localhost:5173');
    header('Access-Control-Allow-Methods: POST, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    header('HTTP/1.1 200 OK');
    exit();
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once __DIR__ . '/database.php';
require_once __DIR__ . '/jwtHandler.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') :
    
    $data = json_decode(file_get_contents('php://input'));

    if (
        !isset($data->email) ||
        !isset($data->password) ||
        empty(trim($data->email)) ||
        empty(trim($data->password))
    ) :
        sendJson(
            422,
            'Silakan isi semua inputan yang wajib diisi & Tidak ada inputan yang boleh kosong.',
            ['required_fields' => ['email', 'password']]
        );
    endif;

    $email = pg_escape_string($connection, trim($data->email));
    $password = trim($data->password);

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) :
        sendJson(422, 'Alamat email salah!');

    elseif (strlen($password) < 4) :
        sendJson(422, 'Kata sandi Anda harus terdiri dari minimal 4 karakter!');
    endif;

    $sql = "SELECT * FROM users WHERE email='$email'";
    $query = pg_query($connection, $sql);
    $row = pg_fetch_assoc($query);

    if ($row === false) sendJson(404, 'Pengguna tidak ditemukan! (Email tidak terdaftar)');
    if (!password_verify($password, $row['password'])) sendJson(401, 'Password Salah!');
    sendJson(200, '', [
        'token' => encodeToken($row['id'])
    ]);
endif;

sendJson(405, 'Metode Permintaan Tidak Valid. Metode HTTP harus POST');

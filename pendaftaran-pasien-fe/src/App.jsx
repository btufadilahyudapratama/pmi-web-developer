import './App.css'
// import Navbar from './components/Navbar';
// import Pin from './components/Pin';
// import Article from './components/Article';
import HomePage from './pages/Index';
import { GlobalContext } from './context';
import { router } from './router';
import { RouterProvider } from 'react-router-dom';

function App() {
  const user = {
    username: "Fadilah Yuda Pratama & Ayuni Afifah",
  }
  return (
    <>
      <div>
        {/* <Navbar /> */}
        {/* <Pin /> */}
        {/* <Article nama="Fadilah Yuda Pratama" titles={['ReactJs', 'flutter']} /> */}
        <GlobalContext.Provider value={user}>
          <RouterProvider router={router}>
            <HomePage />
          </RouterProvider>
        </GlobalContext.Provider>
      </div>
    </>
  )
}

export default App

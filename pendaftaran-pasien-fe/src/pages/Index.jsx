import { useState, useEffect } from "react";
import { AiOutlineClose, AiOutlineMenu } from "react-icons/ai";
import { homeUrl } from "./../apis/loaders";

const Navbar = () => {
  // Negara untuk mengelola visibilitas navbar
  const [nav, setNav] = useState(false);
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    const isAuthenticated = localStorage.getItem("token");
    if (!isAuthenticated) {
      window.location.href = "/";
    }

    const fetchData = async () => {
      try {
        const token = localStorage.getItem("token");
        if (!token) {
          window.location.href = "/";
          // Mengalihkan atau menangani kasus yang tidak diautentikasi
          return;
        }

        const response = await fetch(homeUrl, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        if (response.ok) {
          const responseData = await response.json();
          setUserData(responseData);
        } else if (response.status === 200) {
          // Handle unauthorized access
          console.error("Token authorization missing!");
        } else if (response.status === 401) {
          // Handle unauthorized access
          window.location.href = "/";
        } else if (response.status === 403) {
          // Handle unauthorized access
          console.error("Token authorization missing!");
        } else {
          // Handle other errors
          console.error("Error fetching user data");
        }
      } catch (error) {
        console.error("Terjadi kesalahan:", error);
      }
    };

    fetchData();
  }, []);

  const handleLogout = () => {
    // Perform any additional logout actions (e.g., clearing local storage)
    localStorage.removeItem("token");
    console.log("testt");

    // Redirect to the login page
    window.location.href = "/";
  };

  // Toggle function to handle the navbar's display
  const handleNav = () => {
    setNav(!nav);
  };

  return (
    <>
      <div className="bg-gray-100 flex justify-between items-center h-24 mx-auto px-4 text-black mb-20">
        {/* Logo */}
        <h1 className="w-full text-3xl font-bold text-[#00df9a]">
          {userData && (
            <div>
              {userData.nama}
              {/* Add other user data fields as needed */}
            </div>
          )}
        </h1>

        {/* Desktop Navigation */}
        <ul className="hidden md:flex">
          <li className="p-4 hover:bg-red-100 bg-gray-200 rounded-xl cursor-pointer duration-200 hover:text-black">
            <button onClick={handleLogout}>Logout</button>
          </li>
        </ul>

        {/* Mobile Navigation Icon */}
        <div onClick={handleNav} className="block md:hidden">
          {nav ? <AiOutlineClose size={20} /> : <AiOutlineMenu size={20} />}
        </div>

        {/* Mobile Navigation Menu */}
        <ul
          className={
            nav
              ? "fixed md:hidden left-0 top-0 w-[60%] h-full border-r border-r-gray-900 bg-[#000300] ease-in-out duration-500"
              : "ease-in-out w-[60%] duration-500 fixed top-0 bottom-0 left-[-100%]"
          }
        >
          {/* Mobile Logo */}
          {userData && (
            <div>
              {userData.nama}
              {/* Add other user data fields as needed */}
            </div>
          )}

          {/* Mobile Navigation Items */}

          <li className="p-4 border-b rounded-xl hover:bg-[#00df9a] duration-300 hover:text-black cursor-pointer border-gray-600">
            {userData && (
              <div>
                {userData.nama}
                {/* Add other user data fields as needed */}
              </div>
            )}
          </li>
        </ul>
      </div>
      {/* Konten */}
      <div className="relative flex bg-clip-border md:m-20 rounded-xl bg-white text-gray-700 shadow-md w-full max-w-[48rem] flex-row">
        <div className="relative w-2/5 m-0 overflow-hidden text-gray-700 bg-white rounded-r-none bg-clip-border rounded-xl shrink-0">
          <img
            src="https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixlib=rb-4.0.3&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=1471&amp;q=80"
            alt="card-image"
            className="object-cover w-full h-full"
          />
        </div>
        <div className="p-6">
          <h6 className="block mb-4 font-sans text-base antialiased font-semibold leading-relaxed tracking-normal text-gray-700 uppercase">
            {userData && (
              <div>
                {userData.nama}
                {/* Add other user data fields as needed */}
              </div>
            )}
          </h6>
          <h4 className="block mb-2 font-sans text-2xl antialiased font-semibold leading-snug tracking-normal text-blue-gray-900">
            {userData && (
              <div>
                {userData.email}
                {/* Add other user data fields as needed */}
              </div>
            )}
          </h4>
          <p className="block mb-8 font-sans text-base antialiased font-normal leading-relaxed text-gray-700">
            {userData && (
              <div>
                {userData.alamat}
                {/* Add other user data fields as needed */}
              </div>
            )}
          </p>
          {userData && userData.role === "2" ? (
            ""
          ) : (
            <a href="#" className="inline-block">
              <button
                className="flex items-center gap-2 px-6 py-3 font-sans text-xs font-bold text-center text-gray-900 uppercase align-middle transition-all rounded-lg select-none disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none hover:bg-gray-900/10 active:bg-gray-900/20"
                type="button"
              >
                Detail Pasien
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth="2"
                  className="w-4 h-4"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
                  ></path>
                </svg>
              </button>
            </a>
          )}
        </div>
      </div>

      {/* end konten */}
    </>
  );
};

export default Navbar;

import { useState } from "react";
import ImagePinImage from "./../assets/images/logo-pmi.png";
import { Link } from "react-router-dom";
import { loginUrl } from "./../apis/loaders";

const Login = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const [errors, setErrors] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });

    // Pemeriksaan validasi
    setErrors({
      ...errors,
      [e.target.name]: e.target.value
        ? ""
        : `${e.target.name} wajib di isi ya !!!!`,
    });
  };

  const isFormValid = () => {
    // Periksa apakah ada kesalahan untuk bidang apa pun
    return Object.values(errors).every((error) => !error);
  };

  const handleLogin = async () => {
    try {
      const response = await fetch(loginUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },

        body: JSON.stringify(formData),
      });

      if (response.status === 200) {
        const responseData = await response.json();

        // Simpan token dengan aman (Anda mungkin ingin menggunakan metode yang lebih aman
        localStorage.setItem("token", responseData.token);

        // Menyimpan data pengguna untuk halaman admin
        localStorage.setItem("userData", JSON.stringify(responseData.userData));

        // Redirect to the admin page
        window.location.href = "/home";
      } else if (response.status === 422) {
        const responseData = await response.json();

        // Tampilkan peringatan dengan pesan kesalahan
        alert(responseData.message);

        // Handle validation errors
        setErrors({
          email: responseData.errors.email || "",

          password: responseData.errors.password || "",
        });
      } else if (response.status === 401) {
        // Menangani kasus yang tidak sah (kredensial tidak valid).
        setErrors({
          email: "Email atau password tidak valid.",
          password: "Email atau password tidak valid.",
        });
      }
    } catch (error) {
      console.error("Terjadi kesalahan:", error);
    }
  };
  return (
    <>
      <section className="bg-gray-50 dark:bg-gray-900">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
          <a
            href="#"
            className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
          >
            <img className="" width={150} src={ImagePinImage} alt="My Image" />
          </a>
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Masuk ke akun Anda
              </h1>
              <form className="space-y-4 md:space-y-6">
                <div>
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.email && "border-red-500"
                    }`}
                    placeholder="name@company.com"
                    required=""
                    onChange={handleChange}
                    value={formData.email}
                  />
                  {errors.email && (
                    <p className="text-red-500 text-sm">{errors.email}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.password && "border-red-500"
                    }`}
                    required=""
                    onChange={handleChange}
                    value={formData.password}
                  />
                  {errors.password && (
                    <p className="text-red-500 text-sm">{errors.password}</p>
                  )}
                </div>
                <button
                  type="button"
                  className={`w-full text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800 ${
                    !isFormValid() && "opacity-50 cursor-not-allowed"
                  }`}
                  onClick={handleLogin} // Panggil fungsi handleSubmit dengan mengklik tombol
                  disabled={!isFormValid()}
                >
                  Login
                </button>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                  Belum memiliki akun?
                  <Link
                    to="/regist"
                    className="font-medium mr-2 text-primary-600 hover:underline dark:text-primary-500"
                  >
                    Daftar
                  </Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Login;

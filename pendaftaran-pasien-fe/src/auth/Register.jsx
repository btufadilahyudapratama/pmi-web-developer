import { useState } from "react";
import { Link } from "react-router-dom";
import { registerUrl } from "./../apis/loaders";

const Register = () => {
  // State untuk menyimpan nilai input formulir
  const [formData, setFormData] = useState({
    nama: "",
    email: "",
    password: "",
    nik: "",
    no_telepon: "",
    alamat: "",
  });

  const [errors, setErrors] = useState({
    nama: "",
    password: "",
    nik: "",
    email: "",
    no_telepon: "",
    alamat: "",
  });

  // Menghandle perubahan nilai input
  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });

    // Pemeriksaan validasi
    setErrors({
      ...errors,
      [e.target.name]: e.target.value
        ? ""
        : `${e.target.name} wajib di isi ya !!!!`,
    });
  };

  const isFormValid = () => {
    // Periksa apakah ada kesalahan untuk bidang apa pun
    return Object.values(errors).every((error) => !error);
  };

  // Menghandle submit formulir
  const handleSubmit = async (e) => {
    e.preventDefault();
    // Memanggil API POST
    try {
      const response = await fetch(registerUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      if (response.status === 422) {
        const responseData = await response.json();

        // Tampilkan peringatan dengan pesan kesalahan
        alert(responseData.message);
      } else if (response.status === 200) {
        const responseData = await response.json();

        alert(responseData.message);

        // Arahkan ulang ke halaman login
        window.location.href = "/";
      }
    } catch (error) {
      console.error("Terjadi kesalahan:", error);
    }
  };

  return (
    <>
      <section className="bg-gray-50 dark:bg-gray-900 mt-28">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                form Registrasi
              </h1>
              <form className="space-y-4 md:space-y-6">
                <div>
                  <label
                    htmlFor="nama"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Nama
                  </label>
                  <input
                    type="text"
                    name="nama"
                    id="nama"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.nama && "border-red-500"
                    }`}
                    placeholder="Mr. Robert"
                    required=""
                    onChange={handleChange}
                    value={formData.nama}
                  />
                  {errors.nama && (
                    <p className="text-red-500 text-sm">{errors.nama}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.password && "border-red-500"
                    }`}
                    required=""
                    onChange={handleChange}
                    value={formData.password}
                  />
                  {errors.password && (
                    <p className="text-red-500 text-sm">{errors.password}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="nik"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    NIK
                  </label>
                  <input
                    type="nik"
                    name="nik"
                    id="nik"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.nik && "border-red-500"
                    }`}
                    placeholder="3271011704960001"
                    required=""
                    onChange={handleChange}
                    value={formData.nik}
                  />
                  {errors.nik && (
                    <p className="text-red-500 text-sm">{errors.nik}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.email && "border-red-500"
                    }`}
                    placeholder="name@company.com"
                    required=""
                    onChange={handleChange}
                    value={formData.email}
                  />
                  {errors.email && (
                    <p className="text-red-500 text-sm">{errors.email}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="no_telepon"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    No Telepon
                  </label>
                  <input
                    type="no_telepon"
                    name="no_telepon"
                    id="no_telepon"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.no_telepon && "border-red-500"
                    }`}
                    placeholder="089634808716"
                    required=""
                    onChange={handleChange}
                    value={formData.no_telepon}
                  />
                  {errors.no_telepon && (
                    <p className="text-red-500 text-sm">{errors.no_telepon}</p>
                  )}
                </div>
                <div>
                  <label
                    htmlFor="alamat"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Alamat
                  </label>
                  <textarea
                    type="alamat"
                    name="alamat"
                    id="alamat"
                    className={`bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-red-600 focus:border-red-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${
                      errors.alamat && "border-red-500"
                    }`}
                    placeholder="D3 no.9 Bella Cassa"
                    required=""
                    onChange={handleChange}
                    value={formData.alamat}
                  />
                  {errors.alamat && (
                    <p className="text-red-500 text-sm">{errors.alamat}</p>
                  )}
                </div>
                <button
                  type="button"
                  className={`w-full text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800 ${
                    !isFormValid() && "opacity-50 cursor-not-allowed"
                  }`}
                  onClick={handleSubmit} // Panggil fungsi handleSubmit dengan mengklik tombol
                  disabled={!isFormValid()}
                >
                  Register
                </button>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                  Sudah Punya Akun?
                  <Link
                    to="/"
                    className="font-medium text-red-600 hover:underline dark:text-red-500"
                  >
                    Login
                  </Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Register;

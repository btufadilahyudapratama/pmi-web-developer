const apiUrl = process.env.REACT_APP_API_URL;

// API untuk Login
export const loginUrl = `${apiUrl}/login.php`;
// API untuk Registration
export const registerUrl = `${apiUrl}/register.php`;
// API untuk Home
export const homeUrl = `${apiUrl}/home.php`;

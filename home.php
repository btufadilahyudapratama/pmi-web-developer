<?php
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // Handle preflight OPTIONS request
    header('Access-Control-Allow-Origin: http://localhost:5173');
    header('Access-Control-Allow-Methods: POST, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    header('HTTP/1.1 200 OK');
    exit();
}
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: access');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

require_once __DIR__ . '/database.php';
require_once __DIR__ . '/jwtHandler.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') :
    $headers = getallheaders();
    if (array_key_exists('Authorization', $headers) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) :
        $data = decodeToken($matches[1]);
        $userId = (int) $data;
        if (!is_numeric($data)) sendJson(401, 'Pengguna tidak valid!');

        $sql = "SELECT * FROM users WHERE id='$userId'";
        $query = pg_query($connection, $sql);
        $row = pg_fetch_assoc($query);

        if ($row === false) sendJson(404, 'Pengguna tidak ditemukan!');
        sendJson(200, '', $row);
    endif;
    sendJson(403, "Token Otorisasi Hilang!");
endif;

sendJson(405, 'Token Otorisasi Hilang!');

<?php

// Check if the request method is OPTIONS and handle preflight request
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Origin: http://localhost:5173');
    header('Access-Control-Allow-Methods: GET, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    header('HTTP/1.1 200 OK');
    exit();
}

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: access');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

require_once __DIR__ . '/database.php';
require_once __DIR__ . '/jwtHandler.php';

// Check if the request method is GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Get the authorization token from the headers
    $headers = getallheaders();
    if (array_key_exists('Authorization', $headers) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
        // Decode the token to get user information
        $data = decodeToken($matches[1]);
        $userId = (int) $data;
        
        // Check if the decoded data is a valid number
        if (!is_numeric($userId)) {
            sendJson(401, 'Pengguna tidak valid!');
        }

        // Select all users with role=2 from the database
        $sql = "SELECT * FROM users WHERE role=2";
        $query = pg_query($connection, $sql);

        // Fetch all rows
        $rows = pg_fetch_all($query);

        // Check if any users are found
        if ($rows === false) {
            sendJson(404, 'Tidak ditemukan pengguna dengan peran 2!');
        }

        // Send a JSON response with the user data
        sendJson(200, '', $rows);
    } else {
        sendJson(403, "Token otorisasi hilang!");
    }
} else {
    sendJson(405, 'Metode permintaan tidak valid!');
}

// Function to send JSON response
function sendJson($status, $message, $data = null)
{
    http_response_code($status);
    $response = ['status' => $status, 'message' => $message, 'data' => $data];
    echo json_encode($response);
    exit();
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    // Handle preflight OPTIONS request
    header('Access-Control-Allow-Origin: http://localhost:5173');
    header('Access-Control-Allow-Methods: POST, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    header('HTTP/1.1 200 OK');
    exit();
}
header('Access-Control-Allow-Origin: http://localhost:5173');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

require_once __DIR__ . '/database.php';
require_once __DIR__ . '/sendJson.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $data = json_decode(file_get_contents('php://input'));

    if (
        !isset($data->nama) ||
        !isset($data->password) ||
        !isset($data->email) ||
        !isset($data->nik) ||
        !isset($data->no_telepon) ||
        !isset($data->alamat) ||
        empty(trim($data->nama)) ||
        empty(trim($data->password)) ||
        empty(trim($data->email)) ||
        empty(trim($data->nik)) ||
        empty(trim($data->no_telepon)) ||
        empty(trim($data->alamat))
    ) {
        sendJson(
            422,
            'Silakan isi semua Inputan yang wajib diisi & Tidak ada Inputan yang boleh kosong.',
            ['required_fields' => ['nama', 'email', 'password', 'nik', 'no_telepon', 'alamat']]
        );
    }

    $nama = htmlspecialchars(trim($data->nama));
    $email = trim($data->email);
    $password = trim($data->password);
    $nik = trim($data->nik);
    $no_telepon = trim($data->no_telepon);
    $alamat = trim($data->alamat);

    if (!$connection) {
        sendJson(500, 'Database connection error.');
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        sendJson(422, 'Alamat email salah!');
    } elseif (strlen($password) < 4) {
        sendJson(422, 'Kata sandi Anda minimal harus 4 karakter!');
    } elseif (strlen($nama) < 2) {
        sendJson(422, 'Panjang nama Anda minimal harus 2 karakter!');
    } elseif (strlen($nik) < 16) {
        sendJson(422, 'Panjang nik Anda minimal harus 16 karakter!');
    } elseif (strlen($no_telepon) < 10) {
        sendJson(422, 'No Telpn Anda minimal harus 10 karakter!');
    }

    $hash_password = password_hash($password, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users(nama,password,nik,email,no_telepon,alamat, role) VALUES('$nama', '$hash_password','$nik','$email','$no_telepon','$alamat', 1)";
    $query = pg_query($connection, $sql);

    if ($query) {
        sendJson(200, 'Anda telah berhasil mendaftar.');
    } else {
        sendJson(500, 'Ada yang tidak beres.');
    }
} else {
    sendJson(405, 'Metode Permintaan Tidak Valid. Metode HTTP harus POST');
}

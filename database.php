<?php
$host = 'localhost';
$port = '5432';  // Port default PostgreSQL adalah 5432
$dbname = 'postgres';
$user = 'postgres';
$password = '123456';

$connection = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

if (!$connection) {
    echo "Koneksi ke database gagal: " . pg_last_error();
    exit;
}

// Tutup koneksi saat selesai menggunakan
// pg_close($connection);
?>
